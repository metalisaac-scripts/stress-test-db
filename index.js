const requestTestPromise = require('./request.promise');
const ora = require('ora');
const inquirer = require('inquirer');

const makeStressDbTest = async () => {
  const answer = await inquirer
    .prompt([
      {
        type: 'input',
        name: 'numberOfRequest',
        message: "Cuantas peticiones quieres hacer a la base de datos?",
        validate: function (value) {
          var pass = value.match(
            /^[1-9]\d*$/i
          );
          if (pass) {
            return true;
          }
          return 'El número de peticiones debe ser númerico y mayor a 0.';
        }
      }
    ]);
  const numberOfRequest = answer.numberOfRequest;
  const spinner = ora('Haciendo la prueba de stress...').start();
  requestTestPromise(Number(numberOfRequest)).then(response => {
    spinner.succeed();
    makeStressDbTest();
  }).catch(error => {
    spinner.fail(error);
  });
};

makeStressDbTest();
