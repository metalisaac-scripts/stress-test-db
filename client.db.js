const { Pool } = require('pg');
const connectionJson = require('./connection.db.json');

const client = new Pool(connectionJson);

client.connect();

module.exports = client;
