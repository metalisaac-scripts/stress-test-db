const client = require('./client.db');

const queryText = `
INSERT INTO ctl_clientessanatudeudagenerados(
	idu_cliente,
  nom_cliente,
  imp_saldo,
  idu_producto,
  clv_ciudad,
  des_puntualidad,
  des_puntualidadultimacompra,
  pje_pagadoultimacompra,
  opc_puntualidadaultimoanio,
  fec_ultimacompra,
  fec_cuentaperdida,
  num_regioncobranza,
  num_regiontienda,
  idu_tipocliente
) VALUES(
  $1, 'VIRGINIA LOPEZ CARMONA', 10742, 7,
  127, 'Z', 'N', 10, 0, '2011-08-31', '2013-08-31',
  14, 14, 1
)
`;

async function requestTestPromise(numberRequest) {
  try {
    await client.query('BEGIN');
    const promisesToExecute = [];
    for (let numberRegister = 1; numberRegister <= numberRequest; numberRegister++) {
      const insertValues = [numberRegister];
      promisesToExecute.push(await client.query(queryText, insertValues));
    }
    await Promise.all(promisesToExecute);
    await client.query('ROLLBACK');
    return true;
  } catch (err) {
    console.log('error: ', err);
  }
}

module.exports = requestTestPromise;